
# Project Setup

1. Import the GPCPKit.framework file on your project. We suggest you to check "Copy items if needed" box. Don't forget to check also the different targets you want to be able to use the SDK.
2. Go to the `Settings` tab of your project and scroll down to the `Embedded Binaries` section.
3. Click on the `+` on the bottom and select the GPCPKit framework.

You've complete the setup. You can now start using the GPCPKit SDK.


# GPCPKit Initialisation

## Initialisation

Initialise the GPCPKit by calling the class method:
~~~~
@objc public static func `init`(appId: Int, delegate: GPCPKitDelegate?=nil);
~~~~

Example usage:
~~~~
GPCPKit.init(appId: 1, delegate: self)
~~~~

> **Note**: Ask the Green Panda Team to give you your App ID

## Log Level
Change logLevel to have more or less informations about the GPCPKit SDK by calling the class method:
~~~~
@objc public static func setLog(level: LogLevel);
~~~~

Example usage:
~~~~
GPCPKit.setLog(level: .high)
~~~~

# Placements

## Create a placement

Create a placement by initialising a GPCPPlacement:
~~~~
@objc public init(name: String, type: GPCPPlacementType);
~~~~
Example usage:
~~~~
GPCPPlacement(name: "game_start", type: .fullscreen)
~~~~

> **Note**: Ask the Green Panda Team to give you the available placements for your app.

## Add a placement
Add a placement by calling the class method:
~~~~
@objc public static func addPlacement(_ placement: GPCPPlacement);
~~~~

Example usage:
~~~~
GPCPKit.addPlacement(placement)
~~~~

## Get a placement
Get a created placement by calling the class method:
~~~~
@objc public static func placementWith(name: String, type: GPCPPlacementType) -> GPCPPlacement?;
~~~~

Example usage:
~~~~
let placement = GPCPKit.placementWith(name: "game_start", type: .fullscreen)
~~~~

# Fullscreen Integration

Show a fullscreen placement by calling the class method:
~~~~
@objc public static func show(from: GPCPPlacement) -> Bool;
~~~~

> **Note**: The return value let you know if an ad has been shown or not

Example usage:
~~~~
let adShown = GPCPKit.show(from: placement)
~~~~


# Native Integration
## Integration
A native ad lets you display an ads with your own layout.

Get a native ad by calling the class method:
~~~~
@objc public static func nativeAd(count: Int=1, forPlacement placement: GPCPPlacement) -> [GPCPNativeAd]?;
~~~~

> **Note**: `count` is the number of native ads you want
> Example : if you want to show it in table view

> **Tip**: To get all the available native ads set the `count` param to a big number (ex: 999)

Example usage:
~~~~
let ads = GPCPKit.nativeAd(count: 3, forPlacement: placement)
~~~~

## The attributes

`id: Int` : the ad unique ID
`title: String` : the title of the ad
`icon: UIImage`  : the icon of the ad
`trackingView: UIView?` : the tracking view of the ad (cf next section)

## The Tracking View

The `trackingView` attribute lets the GPCPKit SDK handle the gesture on your own ad created using the native ad.
> **Note** : It must be the superview of your ad or a button where the user will click

# The Delegate

The GPCPKit SDK has a delegate (GPCPAdViewDelegate) attribute. It lets you know the different events than happen with the GPCPKit SDK.

When an ad will be displayed (exclusive for fullscreen):
~~~~
func GPCPKitWillShow(placement: GPCPPlacement, withAdId id: Int, storeId: Int);
~~~~

When an ad has just been displayed (exclusive for fullscreen):
~~~~
func GPCPKitDidShow(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout);
~~~~

When a fullscreen ad has just been closed:
~~~~
func GPCPKitDidClose(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout);
~~~~

When the user has click on an ad:
~~~~
func GPCPKitDidClickOn(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout);
~~~~

