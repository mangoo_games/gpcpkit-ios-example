//
//  ViewController.swift
//  GPCPKit-iOS-Example
//
//  Created by GreenPanda on 13/06/2017.
//  Copyright © 2017 GreenPanda. All rights reserved.
//

import UIKit
import GPCPKit
import StoreKit

class ViewController: UIViewController, GPCPKitDelegate {

    @IBOutlet weak var textFieldNatives: UITextField!
    @IBOutlet weak var viewContainer: UIView!
    
    private var imagesViews = [UIImageView]()
    private var ads = [GPCPNativeAd]()
    
    override var prefersStatusBarHidden: Bool {
        get {
            return true
        }
    }
    
    @IBAction func handleButtonInit() {
        GPCPKit.init(appId: 5, delegate: nil)
        
        GPCPKit.setLog(level: .high)

        GPCPKit.setDelegate(delegate: self)

        GPCPKit.addPlacement(GPCPPlacement(name: "homescreen", type: .native))
        GPCPKit.addPlacement(GPCPPlacement(name: "app_start", type: .fullscreen))
    }

    @IBAction func handleButtonReload() {
        GPCPKit.reload()
    }

    @IBAction func handleButtonShow() {
        if let placement = GPCPKit.placementWith(name: "app_start", type: .fullscreen) {
            _ = GPCPKit.show(from: placement)
        }
    }
    
    @IBAction func handleButtonShowNatives() {
    
        let value = textFieldNatives.text
        let count : Int = value != nil && value! != "" ? Int(value!)! : 1
        
        textFieldNatives.resignFirstResponder()
        
        for image in imagesViews {
            image.removeFromSuperview()
        }
        
        if let placement = GPCPKit.placementWith(name: "homescreen", type: .native) {
            if let ads = GPCPKit.nativeAd(count: count, forPlacement: placement) {
                
                var index = 1.0
                for ad in ads {
                    let imageView = UIImageView(image: ad.icon)
                    imageView.frame = CGRect(x: 40.0, y: 70.0*index, width: 60.0, height: 60.0)
                    ad.trackingView = imageView
                    viewContainer.addSubview(imageView)
                    self.imagesViews.append(imageView)
                    self.ads.append(ad)
                    index += 1.0
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    // MARK: GPCPKitDelegate
    
    func GPCPKitDownloadButtonTitleFor(placement: GPCPPlacement) -> String {
        print("GPCPKit : GPCPKitDownloadButtonTitleFor")
        return "Download"
    }
    
    func GPCPKitWillShow(placement: GPCPPlacement, withAdId id: Int, storeId: Int) {
        print("GPCPKit : GPCPKitWillShow")
    }
    
    func GPCPKitDidShow(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout) {
        print("GPCPKit : GPCPKitDidShow")
    }
    
    func GPCPKitDidClose(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout) {
        print("GPCPKit : GPCPKitDidClose")
    }
    
    func GPCPKitDidClickOn(placement: GPCPPlacement, withAdId id: Int, storeId: Int, layout: GPCPLayout) {
        print("GPCPKit : GPCPKitDidClickOn")
    }
    
    func GPCPKitWillShowStoreView() {
        print("GPCPKit : GPCPKitWillShowStoreView")
    }
    
    func GPCPKitDidCloseStoreView() {
        print("GPCPKit : GPCPKitDidCloseStoreView")
    }
    
    func GPCPKitDidShowStoreView() {
        print("GPCPKit : GPCPKitDidShowStoreView")
    }
    
    func GPCPKitDidFailStoreView() {
        print("GPCPKit : GPCPKitDidFailStoreView")
    }
}

