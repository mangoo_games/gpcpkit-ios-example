//
//  GPCPKit.h
//  GPCPKit
//
//  Created by David Goncalves on 31/07/2017.
//  Copyright © 2017 Green Panda Games. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for GPCPKit.
FOUNDATION_EXPORT double GPCPKitVersionNumber;

//! Project version string for GPCPKit.
FOUNDATION_EXPORT const unsigned char GPCPKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GPCPKit/PublicHeader.h>


